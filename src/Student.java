
public class Student implements Comparable<Student>
{
	String firstName;
	String lastName;
	int Score;
	
	public Student(String firstName, String lastName, int score) 
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		Score = score;
	}
	
	@Override
	public int compareTo(Student other)
	{
		int compareInt = this.firstName.compareTo(other.firstName);
		if (compareInt < 0) return -1;
		if (compareInt > 0) return 1;
		return 0;
	}

	public String getFirstName() 
	{
		return firstName;
	}
	
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
	
	public int getScore() 
	{
		return Score;
	}
	
	public void setScore(int score) 
	{
		Score = score;
	}
	
	@Override
    public String toString() { 
        return ("name: " + this.firstName + " " + this.lastName + " Score: " +  this.Score);
     } 
	
	
}
