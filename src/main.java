import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class main 
{

	public static void main(String[] args) 
	{
		
		//created all student objects
		Student s1 = new Student("Hayden", "Beadles", 1);
		Student s2 = new Student("Gerald", "Carson", 97);
		Student s3 = new Student("Chad", "Curvin", 35);
		Student s4 = new Student("Jayci", "Giles", 44);
		Student s5 = new Student("Ian", "Hale", 16);
		Student s6 = new Student("Joseph", "Hubbard", 22);
		Student s7 = new Student("Jose", "Jara", 77);
		Student s8 = new Student("Jonathan", "Langford", 63);
		Student s9 = new Student("Trevor", "Marsh", 100);
		Student s10 = new Student("Casey", "Mau", 85);
		Student s11 = new Student("Bryant", "Morrill", 58);
		Student s12 = new Student("Mark", "Richardson", 43);
		Student s13 = new Student("Nash", "Stewart", 94);
		Student s14 = new Student("Michael","Zeller", 10);
		
		//add student objects to ArrayList
		List<Student> studentList = new ArrayList<Student>();
		studentList.add(s1);
		studentList.add(s2);
		studentList.add(s3);
		studentList.add(s4);
		studentList.add(s5);
		studentList.add(s6);
		studentList.add(s7);
		studentList.add(s8);
		studentList.add(s9);
		studentList.add(s10);
		studentList.add(s11);
		studentList.add(s12);
		studentList.add(s13);
		studentList.add(s14);
		
		
		
		/*before sorting*/
		System.out.println("Before sort:");
		for (int i = 0; i < 14; i++)
		{
			System.out.println(studentList.get(i).toString());
		}
		
		System.out.println("\n");
		
		/*after sorting*/
		Collections.sort(studentList);
		System.out.println("After sort:");
		for (int i = 0; i < 14; i++)
		{
			System.out.println(studentList.get(i).toString());
		}
		
		System.out.println();
		

		for(int i = 0; i < 13; i++)
		{
			String name1 = studentList.get(i).firstName;
			String name2 = studentList.get(i+1).firstName;
			Group group = new Group();
			group.converSation(name1, name2);
			System.out.println();
			i++;
		}
	}
}
